var searchsecret = {};

searchsecret.$broker = {
    hostName: 'com.mystart.one.newtab.' + chrome.runtime.id,
    campaign_id: "",
    click_id: "",
    gcl_id: "",
    user_id: "",
    config: {},
    getCampaignId: function(callback) {
        chrome.runtime.sendNativeMessage(searchsecret.$broker.hostName, {
            endpoint: 'get-campaign-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    campaign_id: null
                };
            }

            callback(response);
        });
    },
    getConfig: function(callback) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", chrome.extension.getURL('/config.json'), true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    searchsecret.$broker.config = JSON.parse(xhr.responseText);
                    callback();
                }
            }
        }; // Implemented elsewhere.
        xhr.send();
    },
    sendPing: function() {
        console.log("sendPing");
        console.log("sendPing.url=" + searchsecret.$broker.config.ping + searchsecret.$broker.click_id + "&gclid=" + searchsecret.$broker.gcl_id);
        var xhr = new XMLHttpRequest();
        xhr.open("GET", searchsecret.$broker.config.ping + searchsecret.$broker.click_id + "&gclid=" + searchsecret.$broker.gcl_id, true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var response = JSON.parse(xhr.responseText);
                    console.log("ping response.result=" + response.result);
                }
            }
        }; // Implemented elsewhere.
        xhr.send();
    },
    getUserId: function(callback) {
        chrome.runtime.sendNativeMessage(searchsecret.$broker.hostName, {
            endpoint: 'get-user-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    user_id: null
                };
            }

            callback(response);
        });
    },
    guid: function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    },
    checkCookies: function(cookies) {
        for (var i = 0; i < cookies.length; ++i) {
            if (cookies[i].name === searchsecret.$broker.config.cookieName) {
                searchsecret.$broker.click_id = cookies[i].value;
                chrome.storage.local.set({ "click_id": searchsecret.$broker.click_id });
            }
            if (cookies[i].name === "cid") {
                searchsecret.$broker.campaign_id = cookies[i].value;
                chrome.storage.local.set({ "campaign_id": searchsecret.$broker.campaign_id });
                chrome.runtime.setUninstallURL(searchsecret.$broker.config.UNINSTALL_URL + searchsecret.$broker.campaign_id, function() {});
            }
            if (cookies[i].name === "gclid") {
                searchsecret.$broker.gcl_id = cookies[i].value;
                chrome.storage.local.set({ "gcl_id": searchsecret.$broker.gcl_id });
            }
        }
    },
    $init: function() {
        console.log("broker init");
        searchsecret.$broker.getConfig(function() {
            searchsecret.$broker.user_id = searchsecret.$broker.guid();
            searchsecret.$broker.campaign_id = "123";
            chrome.storage.local.get("campaign_id", function(result) {
                if (result.campaign_id)
                    searchsecret.$broker.campaign_id = result.campaign_id;
            });
            chrome.storage.local.get("user_id", function(result) {
                if (result.user_id)
                    searchsecret.$broker.user_id = result.user_id;
            });

            // if (searchsecret.$broker.campaign_id !== "" && searchsecret.$broker.user_id !== "")
            //     return;

            searchsecret.$broker.getCampaignId(function(data) {
                if (!data.error && data.campaign_id) {
                    searchsecret.$broker.campaign_id = data.campaign_id;
                    chrome.storage.local.set({ "campaign_id": searchsecret.$broker.campaign_id });
                    chrome.runtime.setUninstallURL(searchsecret.$broker.config.UNINSTALL_URL + searchsecret.$broker.campaign_id, function() {});
                }
            });

            searchsecret.$broker.getUserId(function(data) {
                if (!data.error && data.user_id) {
                    searchsecret.$broker.user_id = data.user_id;
                    chrome.storage.local.set({ "user_id": searchsecret.$broker.user_id });
                }
            });

            chrome.contextMenus.create({
                "title": searchsecret.$broker.config.title + "'%s'", // 
                "contexts": ["selection"],
                "onclick": searchsecret.$main.searchYahoo
            });

            chrome.cookies.getAll({ url: searchsecret.$broker.config.cookieDomain },
                searchsecret.$broker.checkCookies
            );
        });
        chrome.runtime.onInstalled.addListener(function(details) {
            console.log("details.reason" + details.reason);
            if (details.reason == "install") {
                setTimeout(function() {
                    console.log("searchsecret.$broker.click_id=" + searchsecret.$broker.click_id);
                    console.log("searchsecret.$broker.glc_id=" + searchsecret.$broker.gcl_id);
                    if (searchsecret.$broker.click_id !== "" || searchsecret.$broker.gcl_id !== "")
                        searchsecret.$broker.sendPing();
                    searchsecret.$analytics.trackEvent("Runtime_Install", searchsecret.$broker.campaign_id, " ", " ");
                    chrome.runtime.setUninstallURL(searchsecret.$broker.config.UNINSTALL_URL + searchsecret.$broker.campaign_id, function() {});
                }, 3000);
            }
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    searchsecret.$broker.$init();
});