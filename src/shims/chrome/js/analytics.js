if (!searchsecret)
    var searchsecret = {};

searchsecret.$analytics = {
    $ga: {
        _AnalyticsCode: "UA-91618087-2",
        init: function() {},
        trackPage: function(page, customUrl) {
            var request = new XMLHttpRequest();
            var message =
                "v=1&tid=" + searchsecret.$analytics.$ga._AnalyticsCode + "&cid=" + searchsecret.$broker.user_id + "&aip=1" +
                "&cn=" + searchsecret.$events.campaign_id + "&ds=add-on&t=pageview&dp=" + page + "&dr=" + customUrl;

            console.log("trackPage.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        },
        trackEvent: function(category, action, label, value) {
            var request = new XMLHttpRequest();
            var message = "tid=" + searchsecret.$analytics.$ga._AnalyticsCode + "&cid=" + searchsecret.$broker.user_id;
            message += "&cn=" + searchsecret.$broker.campaign_id + "&t=event&ec=" + category + "&v=1";

            if (action && action !== '')
                message += "&ea=" + action;

            if (label && label !== '')
                message += "&el=" + label;

            console.log("ga.trackEvent.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        }
    },
    $piwik: {
        url: "https://analytics.vmn.net/",
        cid: 12,
        pwTracker: {},
        getPwTracker: function(baseUrl, siteId) {
            var u = baseUrl.replace(/\/$/, "");
            var tracker = function() {
                // arguments does not implement Array common function (shift, slice....)
                // Piwik make use of these function on _paq instance thus the necessity 
                // of casting arguments as Array
                (window._paq = window._paq || []).push(Array.prototype.slice.call(arguments));
            };
            tracker = Piwik.getTracker(u + '/piwik.php', siteId);
            return tracker;
        },
        init: function() {
            searchsecret.$analytics.$piwik.pwTracker = searchsecret.$analytics.$piwik.getPwTracker(searchsecret.$analytics.$piwik.url, searchsecret.$analytics.$piwik.cid);
        },
        trackPage: function(page, customUrl) {
            var url = 'https://searchsecret.net/?pk_campaign=' + searchsecret.$broker.campaign_id;
            searchsecret.$analytics.$piwik.pwTracker.setUserId(searchsecret.$broker.user_id);
            searchsecret.$analytics.$piwik.pwTracker.setCustomUrl(url);
            searchsecret.$analytics.$piwik.pwTracker.setDocumentTitle(page); // replace wlcome with the page name
            searchsecret.$analytics.$piwik.pwTracker.trackPageView(); // replace welcome with page name
        },
        trackEvent: function(category, action, label, value) {
            var url = 'https://searchsecret.net/?pk_campaign=' + searchsecret.$broker.campaign_id;
            console.log("piwik.trackEvent.url=" + url);
            searchsecret.$analytics.$piwik.pwTracker.setUserId(searchsecret.$broker.user_id);
            searchsecret.$analytics.$piwik.pwTracker.setCustomUrl(url);
            searchsecret.$analytics.$piwik.pwTracker.trackEvent(category, action, label, value);
        }
    },
    trackPage: function(page, customUrl) {
        searchsecret.$analytics.$ga.trackPage(page, customUrl);
        searchsecret.$analytics.$piwik.trackPage(page, customUrl);
    },
    trackEvent: function(category, action, label, value) {
        searchsecret.$analytics.$ga.trackEvent(category, action, label, value);
        searchsecret.$analytics.$piwik.trackEvent(category, action, label, value);
        console.log("trackEvent:" + category + "|" + action + "|" + label + "|" + value);
    }
};

document.addEventListener("DOMContentLoaded", function() {
    searchsecret.$analytics.$ga.init();
    searchsecret.$analytics.$piwik.init();
});