if (chrome.extension) {
    var port = chrome.extension.connect({ name: "searchsecretly" });
}

function clearHistory() {
    chrome.tabs.create({ url: "chrome://settings/clearBrowserData", active: true });
    port.postMessage({ message: "send-stats", value: "popup-ClearHistory" });
}

function faq() {
    chrome.tabs.create({ url: "https://searchsecretly.net/faq", active: true });
    port.postMessage({ message: "send-stats", value: "popup-Faq" });
}

function searchYahoo() {
    port.postMessage({ message: "search-yahoo", value: document.getElementById("term").value });
}

function searchGoogle() {
    port.postMessage({ message: "search-google", value: document.getElementById("term").value });
}

function onLoad() {
    port.postMessage({ message: "send-stats", value: "popup-Open" });
    window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
    document.getElementById("term").focus();
    document.getElementById("search-yahoo").onclick = searchYahoo;
    document.getElementById("term").onkeypress = function(evt) {
        if (evt.keyCode === 13) {
            searchYahoo();
        }
    };
    document.getElementById("search-google").onclick = searchGoogle;
    document.getElementById("faq").onclick = faq;
    document.getElementById("clh").onclick = clearHistory;
}
document.addEventListener("DOMContentLoaded", onLoad, false);