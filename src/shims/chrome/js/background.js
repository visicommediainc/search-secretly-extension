searchsecret.$main = {
    beatLastUpdate: 0,
    searchYahoo: function(ocd) {
        if (typeof ocd === "object")
            text = ocd.selectionText;
        else
            text = ocd;
        var contextSearch = searchsecret.$broker.config.yahoo + searchsecret.$broker.campaign_id + '_&q=' + text;
        chrome.windows.create({ "url": contextSearch, "incognito": true });
        searchsecret.$analytics.trackEvent("Context_Search", "Yahoo", searchsecret.$broker.campaign_id, " ");
    },

    searchGoogle: function(ocd) {
        if (typeof ocd === "object")
            text = ocd.selectionText;
        else
            text = ocd;
        chrome.windows.create({ "url": searchsecret.$broker.config.google + text, "incognito": true });
        searchsecret.$analytics.trackEvent("Context_Search", "Google", searchsecret.$broker.campaign_id, " ");
    },
    onMessage: function(port) {
        port.onMessage.addListener(function(request) {
            switch (request.message) {
                case 'search-yahoo':
                    {
                        searchsecret.$main.searchYahoo(request.value);
                        searchsecret.$analytics.trackEvent("Action_Search", "Yahoo", searchsecret.$broker.campaign_id, " ");
                    }
                    break;
                case 'search-google':
                    {
                        searchsecret.$main.searchGoogle(request.value);
                        searchsecret.$analytics.trackEvent("Action_Search", "Google", searchsecret.$broker.campaign_id, " ");
                    }
                    break;
                case 'send-stats':
                    {
                        searchsecret.$analytics.trackEvent(request.value, " ", " ", " ");
                    }
                    break;
            }
        });
    },
    onBeforeRequest: function(details) {
        var url = details.url;
        if (url.indexOf("_chromeextension_ss_") != -1 && url.indexOf("__campaign__") != -1 && url.indexOf("searchsecretly.net") != -1) {
            url = url.replace("_chromeextension_ss_", "");
            url = url.replace("__campaign__", searchsecret.$broker.campaign_id);
            params = encodeURIComponent(url.split("?")[1].split('&q=')[0]);
            console.log("searchParams:" + params);
            searchsecret.$analytics.trackEvent("Action_Search", "Omnibar_Search", searchsecret.$broker.campaign_id, params);
            return { redirectUrl: url };
        }
    },
    onMessageExternal: function(request, sender, callback) {
        if (request.message === 'version') {
            callback("__version__");
        }
    },
    $init: function() {
        console.log("init");
        var now = parseInt(new Date().getTime() / 1000);
        chrome.storage.local.get("beatLastUpdate", function(result) {
            searchsecret.$main.beatLastUpdate = (typeof result.beatLastUpdate !== "undefined") ? result.beatLastUpdate : 0;
            beatUpdateDue = parseInt(searchsecret.$main.beatLastUpdate) + parseInt(searchsecret.$main.beatInterval / 1000);
            if (beatUpdateDue > now) {
                console.log("Too soon to send heart beat!");
                return;
            }
            setTimeout(function() {
                searchsecret.$analytics.trackEvent("Heartbeat_DailyUser", searchsecret.$broker.campaign_id, " ", " ");
            }, 1000);
            chrome.storage.local.set({ "beatLastUpdate": now }, function() {
                searchsecret.$main.beatLastUpdate = now;
                console.log("beatLastUpdate sent at " + now);
            });
        });
    }
};

chrome.runtime.onConnect.addListener(searchsecret.$main.onMessage);
chrome.omnibox.onInputEntered.addListener(function(omniText) {
    searchsecret.$main.searchYahoo(omniText);
});
chrome.runtime.onMessageExternal.addListener(searchsecret.$main.onMessageExternal);
chrome.webRequest.onBeforeRequest.addListener(searchsecret.$main.onBeforeRequest, { urls: ["<all_urls>"] }, ["blocking"]);


document.addEventListener("DOMContentLoaded", function() {
    searchsecret.$main.$init();
});