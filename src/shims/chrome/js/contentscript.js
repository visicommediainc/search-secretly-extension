    if (chrome.extension)
    {
        var port = chrome.extension.connect({name: "searchsecretly"});
    }
    
    function searchIcognito(text)
    {
        try
        {
            text = document.getElementById("q").value;
            port.postMessage( {message: "search-yahoo",  value: text});
           return false;
 
        }
        catch(e)
        {
            //alert(e.message);
        }
    }

    function init() {
        if(document.location.hostname === "searchsecretly.net")
        {
            if (document.getElementById("homepage_app_searchbox"))
            {
                document.getElementById("homepage_app_searchbox").onsubmit = function(evt){
                    searchIcognito();
                    return false;
                };
            }
            if (document.getElementsByClassName("buttonlabel") && document.getElementsByClassName("buttonlabel").length > 0)
            {
                document.getElementsByClassName("buttonlabel")[0].onclick = function(evt){
                    evt.preventDefault(); 
                    searchIcognito();
                    return false;
                };
            }
        }
    }

document.addEventListener("DOMContentLoaded", function() {
   init();
}, false);