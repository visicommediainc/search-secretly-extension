module.exports = function(grunt) {
    "use strict";
    var os = require('os'),
        osType = os.type();
    require("load-grunt-tasks")(grunt);
    var PATHS = {
        js: [
            '*.json',
            '*.js',
            'src/**/*.json',
            'src/**/*.js'
        ]
    };
    var pkg = grunt.file.readJSON('package.json');
    var version = pkg.version;
    var short = pkg.short;
    var description = pkg.description;
    var campaign = pkg.compaignId;

    var option = grunt.file.readJSON('config/visicom/config.json');
    var name = option.name;
    var analytics = option.analytics;
    var target = grunt.option('target');
    var browser = grunt.option('browser');
    if (typeof browser === "undefined")
        browser = "chrome";

    var target = grunt.option('target');
    var banner = '/*! ' + name + ' V<%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd") %>) */\n';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['dist', 'temp', 'uglify'],
        jshint: {
            js: [
                "src/**/*.js"
            ],
            options: {
                esnext: true,
                reporter: require('jshint-summary'),
                ignores: ['Gruntfile.js', 'src/**/jquery*.js', 'src/**/perfect*.js', 'src/**/lzma-d.js', 'src/**/piwik.lib.js'],
            },
            all: PATHS.js,
            watched: { src: [] }
        },
        copy: {
            main: {
                files: [
                    { expand: true, cwd: 'src/shims/chrome/', src: ['**'], dest: 'temp/' + name + '/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/chrome/', src: ['manifest.json'], dest: 'temp/' + name + '/', filter: 'isFile' }
                ]
            }
        },
        uglify: {
            dev: {
                options: {
                    banner: banner,
                    mangle: false,
                    compress: false,
                    beautify: true
                },
                files: [{
                    expand: true,
                    cwd: 'temp/' + name + '/js',
                    src: ['*.js'],
                    dest: 'temp/' + name + '/js',
                }]
            },
            prod: {
                options: {
                    banner: banner,
                    mangle: true,
                    compress: true,
                    beautify: false
                },
                files: [{
                    expand: true,
                    cwd: 'temp/' + name + '/js',
                    src: ['*.js', '!piwik.*'],
                    dest: 'temp/' + name + '/js',
                    ext: ".js"
                }]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'src/shims/chrome/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'temp/' + name + '/css',
                    ext: '.css'
                }]
            }
        },
        'string-replace': {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src/shims/chrome/',
                    src: 'manifest.json',
                    dest: 'temp/' + name
                }],
                options: {
                    replacements: [{
                            pattern: /__name__/g,
                            replacement: name
                        },
                        {
                            pattern: /__version__/g,
                            replacement: version
                        },
                        {
                            pattern: /__short__/g,
                            replacement: short
                        },
                        {
                            pattern: /__description__/g,
                            replacement: description
                        }
                    ]
                }
            }
        },
        zip: {
            chrome: {
                cwd: 'temp/' + name + '/',
                src: ['temp/' + name + '/**/*', ],
                dest: 'public/' + short + '_' + version + '.zip'
            }
        }
    });

    // Default task(s).
    grunt.registerTask('no-watch', ['clean', 'jshint:all']);
    grunt.registerTask('chrome', ['copy', 'uglify:' + ((target === "prod") ? "prod" : "dev"), 'cssmin', 'string-replace', 'zip']);
    grunt.registerTask('default', ['no-watch', 'chrome']);
};